set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=4.45;
select
CREATION_DATE::date as fecha
,'BR' as pais
,'Dlocal' as adquiriente
,SUM(GROSS_LOCAL_AMOUNT) as VENTA_LOCAL
,SUM(GROSS_LOCAL_AMOUNT)/$ex_rate as VENTA_USD
,SUM(FEE_LOCAL_AMOUNT) as FEE_LOCAL
,SUM(FEE_LOCAL_AMOUNT)/$ex_rate as FEE_USD
from SIMETRIKDB_PUBLIC.br__adquirencia__dlocal__pix
where ROW_TYPE ilike any ('%PAYMENT%','%CAPTURE%')
and SKT__UNIQUENESS = 1
and CREATION_DATE::date between $fecha_ini::date and $fecha_fin::date
group by 1,2,3
order by 1,2,3

