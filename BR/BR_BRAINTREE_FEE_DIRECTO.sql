set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=4.45;
with base as (
select  SKT_CREATED_AT::date as FECHA, SUM(amount_submitted_for_settlement) as Venta,SUM(SERVICE_FEE) as FEE
from SIMETRIKDB_PUBLIC.BR_ADQ_BRAINTREE_8116
where SKT_CREATED_AT::date between $fecha_ini::date and $fecha_fin::date
group by 1
  
union all

select SKT__CREATED_DATETIME::date as FECHA, SUM(amount_submitted_for_settlement) as Venta,SUM(SERVICE_FEE) as FEE
from SIMETRIKDB_PUBLIC.br__adquirencia__braintree
where SKT__CREATED_DATETIME::date between $fecha_ini::date and $fecha_fin::date
and SKT__UNIQUENESS= 1
group by 1
order by 1
)
select
fecha
,'BR' as pais
,'Braintree' as adquiriente
,SUM(venta) as VENTA_LOCAL
,SUM(venta)/$ex_rate as VENTA_USD
,SUM(FEE) as FEE_LOCAL
,SUM(FEE)/$ex_rate as FEE_USD
from base
group by 1,2,3
order by 1,2,3
