set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=4.45;
select distinct
SKT__TRANSACTION_COMPLETION_DATE::date  as FECHA
,'BR' as pais
,'Paypal' as adquiriente
,transaction_debit_or_credit
,CASE when TRANSACTION_DEBIT_OR_CREDIT='DR' then -SUM(try_to_numeric(gross_transaction_amount)/100) else SUM(try_to_numeric(gross_transaction_amount)/100) end as VENTA_LOCAL
,(CASE when TRANSACTION_DEBIT_OR_CREDIT='DR' then -SUM(try_to_numeric(gross_transaction_amount)/100) else SUM(try_to_numeric(gross_transaction_amount)/100) end)/$ex_rate as VENTA_USD
,CASE when TRANSACTION_DEBIT_OR_CREDIT='DR' then -SUM(FEE_AMOUNT/100) else SUM(FEE_AMOUNT/100) end as FEE_LOCAL
,(CASE when TRANSACTION_DEBIT_OR_CREDIT='DR' then -SUM(FEE_AMOUNT/100) else SUM(FEE_AMOUNT/100) end)/$ex_rate as FEE_USD
from simetrikdb_public.br__adquirencia__paypal
--where adq_amount is not null
where SKT__TRANSACTION_COMPLETION_DATE::date between $fecha_ini::date and $fecha_fin::date
and skt__UNIQUENESS =1
group by 1,2,3,4
order by 1,2,3
