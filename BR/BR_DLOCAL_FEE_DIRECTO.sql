select CREATION_DATE::date as fecha
,SUM(GROSS_LOCAL_AMOUNT) as sales
,SUM(FEE_LOCAL_AMOUNT) as fees 
from simetrikdb_public.BR__dlocal__settlement__report
group by 1
order by 1
