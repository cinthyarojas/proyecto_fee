set fecha_ini='2022-01-01';
set fecha_fin='2022-03-31';
--NO_CACHE;
with base as (
  -----COBROS---  
SELECT *
from(
select 
  o.FECHA::date as mes
  ,'COBROS_ORDERS' as tipo
  , p.reference_model
  ,coalesce(c1.acquirer, coalesce(c2.acquirer, coalesce(c3.acquirer, coalesce (c4.acquirer, 'SIN IDENTIFICAR')))) AS adquiriente
  ,case when od.vertical = 'RAPPI TRAVEL' then 'Travel' else 'Core' end as vertical
--   od.vertical,
,p.id as purchase_id
,t.id as transaction_id
,p.reference_id
,p.first_six_digits AS BIN
,p.last_four_digits
,t.authorization_code as Auth_code
,a.card_type
,t.gateway_transaction_id
,t.GATEWAY_TOKEN
,t.GATEWAY_TYPE
,p.gateway_name
,p.amount as total
from global_payments.MX_cobros_cc o 
  left join global_finances.mx_order_details od on o.order_id = od.order_id
join mx_pglr_ms_payment_transaction_public.purchases p on o.table_id = p.id and o.tipo in ('COBROS')
left join mx_pglr_ms_payment_transaction_public.transactions t on p.transaction_id = t.id
left join mx_PG_MS_USER_ASSET_ACCOUNT_PUBLIC.ACCOUNTS_OFUSCATED a on a.id::text = p.payment_method_token::text 
left join  rappi_payments_staging.comercios_rappi c1 on c1.type = 'Tradicional' and a.card_type = c1.card_type and t.GATEWAY_TOKEN = c1.GATEWAY_TOKEN and c1.country = 'MX'
  left join  rappi_payments_staging.comercios_rappi c2 on c2.type = 'Sin Tarjeta' and a.card_type is null and t.GATEWAY_TOKEN = c2.GATEWAY_TOKEN and c2.country = 'MX'
  left join  rappi_payments_staging.comercios_rappi c3 on c3.type = 'Por Gateway' and a.card_type is null and t.GATEWAY_TYPE = c3.GATEWAY_TOKEN and c3.country = 'MX'
  left join rappi_payments_staging.comercios_rappi c4 on c4.type = 'Gateway-Tarjeta' and a.card_type = c4.card_type and t.GATEWAY_TYPE = c4.GATEWAY_TOKEN and c4.country = 'MX'
where o.FECHA::date between   $fecha_ini::date and $fecha_fin::date
--group by 1,2,3,4 ,5,6,7,8

union all

  -----REFUNDS-----
  
select 
 o.FECHA::date as mes
,'REFUNDS_ORDERS' as tipo  
,r.reference_model
,coalesce(c1.acquirer, coalesce(c2.acquirer, coalesce(c3.acquirer, coalesce (c4.acquirer, 'SIN IDENTIFICAR')))) AS adquiriente
,case when od.vertical = 'RAPPI TRAVEL' then 'Travel' else 'Core' end as vertical
,r.id as purchase_id
,t.id as transaction_id
,r.reference_id
,r.first_six_digits AS BIN
,r.last_four_digits
,t.authorization_code as Auth_code
,a.card_type
,t.gateway_transaction_id
,t.GATEWAY_TOKEN
,t.GATEWAY_TYPE
,r.gateway_name
,-r.amount as total
from global_payments.MX_cobros_cc o 
left join global_finances.mx_order_details od on o.order_id = od.order_id
join mx_pglr_ms_payment_transaction_public.refund r on o.table_id = r.id and o.tipo in ('REFUNDS_REINTEGRO','REFUNDS_GASTO')
left join mx_pglr_ms_payment_transaction_public.purchases p on r.purchase_id = p.id
left join mx_pglr_ms_payment_transaction_public.transactions t on r.transaction_id = t.id
left join mx_PG_MS_USER_ASSET_ACCOUNT_PUBLIC.ACCOUNTS_OFUSCATED a on a.id::text = p.payment_method_token::text 
left join  rappi_payments_staging.comercios_rappi c1 on c1.type = 'Tradicional' and a.card_type = c1.card_type and t.GATEWAY_TOKEN = c1.GATEWAY_TOKEN and c1.country = 'MX'
left join  rappi_payments_staging.comercios_rappi c2 on c2.type = 'Sin Tarjeta' and a.card_type is null and t.GATEWAY_TOKEN = c2.GATEWAY_TOKEN and c2.country = 'MX'
left join  rappi_payments_staging.comercios_rappi c3 on c3.type = 'Por Gateway' and a.card_type is null and t.GATEWAY_TYPE = c3.GATEWAY_TOKEN and c3.country = 'MX'
left join rappi_payments_staging.comercios_rappi c4 on c4.type = 'Gateway-Tarjeta' and a.card_type = c4.card_type and t.GATEWAY_TYPE = c4.GATEWAY_TOKEN and c4.country = 'MX'
where o.FECHA::date between   $fecha_ini::date and $fecha_fin::date
--group by 1,2,3,4 ,5,6,7,8
  
union all 
  
  -----COBROS OTHERS---  
select 
p.created_at::date as mes
,'COBROS_OTHERS' as tipo
,CASE when p.reference_model = 'RappiPay_CC' then 'RappiPay' else p.reference_model end as reference_model
,coalesce(c1.acquirer, coalesce(c2.acquirer, coalesce(c3.acquirer, coalesce (c4.acquirer, 'SIN IDENTIFICAR')))) AS adquiriente
,'' as vertical
,p.id as purchase_id
,t.id as transaction_id
,p.reference_id
,p.first_six_digits AS BIN
,p.last_four_digits
,t.authorization_code as Auth_code
,a.card_type
,t.gateway_transaction_id
,t.GATEWAY_TOKEN
,t.GATEWAY_TYPE
,p.gateway_name
,p.amount as total
from mx_PGLR_MS_PAYMENT_TRANSACTION_PUBLIC.purchases p 
left join mx_PGLR_MS_PAYMENT_TRANSACTION_PUBLIC.transactions t on p.transaction_id = t.id
left join mx_PG_MS_USER_ASSET_ACCOUNT_PUBLIC.ACCOUNTS_OFUSCATED a on a.id::text = p.payment_method_token::text 
left join  rappi_payments_staging.comercios_rappi c1 on c1.type = 'Tradicional' and a.card_type = c1.card_type and t.GATEWAY_TOKEN = c1.GATEWAY_TOKEN and c1.country = 'MX'
left join  rappi_payments_staging.comercios_rappi c2 on c2.type = 'Sin Tarjeta' and a.card_type is null and t.GATEWAY_TOKEN = c2.GATEWAY_TOKEN and c2.country = 'MX'
left join  rappi_payments_staging.comercios_rappi c3 on c3.type = 'Por Gateway' and a.card_type is null and t.GATEWAY_TYPE = c3.GATEWAY_TOKEN and c3.country = 'MX'
left join rappi_payments_staging.comercios_rappi c4 on c4.type = 'Gateway-Tarjeta' and a.card_type = c4.card_type and t.GATEWAY_TYPE = c4.GATEWAY_TOKEN and c4.country = 'MX'
where p.created_at::date between   $fecha_ini::date and $fecha_fin::date
and p.reference_model <> 'Order'
AND p.amount <> 0
and p.state_id in (4,9)
--group by 1,2,3,4,5,6,7

union all

  -----REFUNDS OTHERS-----
  
select 
r.created_at::date as mes
,'REFUNDS_OTHERS' as tipo
,CASE when r.reference_model = 'RappiPay_CC' then 'RappiPay' else r.reference_model end as reference_model
,coalesce(c1.acquirer, coalesce(c2.acquirer, coalesce(c3.acquirer, coalesce (c4.acquirer, 'SIN IDENTIFICAR')))) AS adquiriente
,'' as vertical
,r.id as purchase_id
,t.id as transaction_id
,r.reference_id
,r.first_six_digits AS BIN
,r.last_four_digits
,t.authorization_code as Auth_code
,a.card_type
,t.gateway_transaction_id
,t.GATEWAY_TOKEN
,t.GATEWAY_TYPE
,r.gateway_name
,-r.amount as total
from mx_PGLR_MS_PAYMENT_TRANSACTION_PUBLIC.refund r 
left join mx_PGLR_MS_PAYMENT_TRANSACTION_PUBLIC.purchases p on r.purchase_id = p.id
left join mx_PGLR_MS_PAYMENT_TRANSACTION_PUBLIC.transactions t on r.transaction_id = t.id
left join mx_PG_MS_USER_ASSET_ACCOUNT_PUBLIC.accounts_ofuscated a on a.id::text = p.payment_method_token::text 
left join  rappi_payments_staging.comercios_rappi c1 on c1.type = 'Tradicional' and a.card_type = c1.card_type and t.GATEWAY_TOKEN = c1.GATEWAY_TOKEN and c1.country = 'MX'
left join  rappi_payments_staging.comercios_rappi c2 on c2.type = 'Sin Tarjeta' and a.card_type is null and t.GATEWAY_TOKEN = c2.GATEWAY_TOKEN and c2.country = 'MX'
left join  rappi_payments_staging.comercios_rappi c3 on c3.type = 'Por Gateway' and a.card_type is null and t.GATEWAY_TYPE = c3.GATEWAY_TOKEN and c3.country = 'MX'
left join rappi_payments_staging.comercios_rappi c4 on c4.type = 'Gateway-Tarjeta' and a.card_type = c4.card_type and t.GATEWAY_TYPE = c4.GATEWAY_TOKEN and c4.country = 'MX'
where r.created_at::date between   $fecha_ini::date and $fecha_fin::date
  and r.reference_model <> 'Order'
  AND r.amount <> 0
  and r.state_id in (4)) as MX_adquirencia
    
WHERE adquiriente in ('D_Local')
and tipo in ('COBROS_ORDERS','COBROS_OTHERS')  
  ),
  
   -----BASE AMEX----- 
amex as (
    select TRANSACTION_DATE, APPROVAL_CODE, left(CARDMEMBER_ACCOUNT_NUMBER,6) as BIN, 
       right(CARDMEMBER_ACCOUNT_NUMBER,4) as Last_four_digits,TRANSACTION_AMOUNT
    FROM SIMETRIKDB_PUBLIC.MX__ADQUIRENCIA__AMEX
    where SKT__UNIQUENESS = 1
)

   -----BASE FINAL----- 
SELECT to_char(base.mes,'yyyy-mm-dd') as mes, 
SUM(amex.TRANSACTION_AMOUNT) as Amex_Ventas, 
SUM(base.total) as Rappi_Ventas,
(Amex_Ventas/Rappi_Ventas) as Conciliacion
--SELECT BASE.*, '-' AS SEPARADOR,AMEX.*
from base
left join amex on amex.TRANSACTION_AMOUNT = base.total
               and amex.last_four_digits = base.LAST_FOUR_DIGITS 
               and amex.BIN = base.BIN
               --and amex.APPROVAL_CODE = base.Auth_code
where mes between $fecha_ini::date and $fecha_fin::date
AND 1=1 and 1=1
Group by 1
Order by 1