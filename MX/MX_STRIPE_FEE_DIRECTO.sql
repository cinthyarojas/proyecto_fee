set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=20;

with base_stripe as(

select SKT__CREATED::date as fecha,AMOUNT as venta, FEE AS COMISION, NET AS NETO,TYPE
from SIMETRIKDB_PUBLIC.mx__adquirencia__stripe
WHERE SKT__UNIQUENESS=1

union all

select SKT__CREATED::date as fecha,AMOUNT as venta, FEE AS COMISION, NET AS NETO,TYPE
from SIMETRIKDB_PUBLIC.mx__adquirencia__stripe__market
WHERE SKT__UNIQUENESS=1

union all

select SKT__CREATED::date as fecha,AMOUNT as venta, FEE AS COMISION, NET AS NETO,TYPE
from SIMETRIKDB_PUBLIC.mx__adquirencia__stripe__prime
WHERE SKT__UNIQUENESS=1

union all

select SKT__CREATED::date as fecha,AMOUNT as venta, FEE AS COMISION, NET AS NETO,TYPE
from SIMETRIKDB_PUBLIC.mx__adquirencia__stripe__safe
WHERE SKT__UNIQUENESS=1

)

select 
fecha
,'MX' as pais
,'Stripe' as adquiriente
,TYPE
,SUM(venta/100) as VENTA_LOCAL
,SUM(venta/100)/$ex_rate as VENTA_USD
,SUM(COMISION/100) as FEE_LOCAL
,SUM(COMISION/100)/$ex_rate as FEE_USD
--, SUM(NETO/100)
from base_stripe
where FECHA::DATE between $fecha_ini::date and $fecha_fin::date
GROUP BY 1,2,3,4
ORDER BY 1,2,3,4
