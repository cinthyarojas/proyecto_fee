set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=20;
SELECT approved_date::date as fecha
,'MX' as pais
,'Dlocal' as adquiriente
,sum(GROSS_LOCAL_AMOUNT) AS VENTA_LOCAL
,sum(GROSS_LOCAL_AMOUNT)/$ex_rate AS VENTA_USD
,sum(fee_local_amount) as FEE_LOCAL
,sum(fee_local_amount)/$ex_rate as FEE_USD
from SIMETRIKDB_PUBLIC.mx_adquirencia_dlocal_7352
where approved_date::date between $fecha_ini::date and $fecha_fin::date
and SKT__UNIQUENESS=1
group by 1,2,3
order by 1,2,3
