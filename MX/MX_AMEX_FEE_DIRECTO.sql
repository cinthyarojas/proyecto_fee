set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=20;
SELECT 
PAYMENT_DATE::date as fecha
,'MX' as pais
,'AMEX' as adquiriente
,sum(DISCOUNT_AMOUNT) as total_dto
,0 as VENTA_LOCAL
,0 as VENTA_USD
,(sum(discount_amount)/1.16) as FEE_LOCAL
,((sum(discount_amount)/1.16))/$ex_rate as FEE_USD
,FEE_LOCAL*0.16 as IVA
FROM SIMETRIKDB_PUBLIC.mx__fees__amex
where skt__uniqueness=1
and payment_date::Date BETWEEN $fecha_ini::date AND $fecha_fin::date
group by 1,2,3
order by 1
