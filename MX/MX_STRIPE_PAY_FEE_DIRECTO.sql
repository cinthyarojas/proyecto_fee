set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=20;
select 
SKT__CREATED::date as fecha
,'MX' as pais
,'Stripe_pay' as adquiriente
,STATUS
,TYPE
,SUM(AMOUNT) as VENTA_LOCAL
,SUM(AMOUNT)/$ex_rate as VENTA_USD
,SUM(FEE) as  FEE_LOCAL
,SUM(FEE)/$ex_rate as  FEE_USD
from 
SIMETRIKDB_PUBLIC.MX__ADQUIRENCIA__STRIPE__RAPPIPAY
where SKT__CREATED::date between $fecha_ini::date and $fecha_fin::date
group by 1,2,3,4,5
order by 1,2,3