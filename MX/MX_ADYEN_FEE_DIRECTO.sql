--'["RappiMX","RappiMX_safe","RappiMX_Loyalty","RappiMX_Validations","RappiMX_Travel","RappiPayMX"]';
-----1.horario de verano Amsterdam--------
set hora_verano_ini='2021-03-28';
set hora_verano_fin='2021-10-31';
set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set tax_authorised=0.004;
set tax_received=0.006;
set tax_mgmt_service=0.002;
set USD_CHANGE=20;
--set acquirer='RappiMX';
set variable1 = '["RappiMX","RappiMX_safe","RappiMX_Loyalty","RappiMX_Validations","RappiMX_Travel","RappiPayMX"]';

with adyen_fees as (
  
-------------Processing Fees------------
  
select
ADYEN_TIME::date as fecha
,merchant_account
,'01_Processing Fees' as Invoice_field
,'01_Processing Fees_Transaction Fee' as Invoice
,RECORD_TYPE
,COUNT(*) COUNT
,CASE WHEN RECORD_TYPE = 'SentForSettle' then COUNT(*)*$tax_authorised*$USD_CHANGE else COUNT(*)*$tax_received*$USD_CHANGE end as FEE_LOCAL
,CASE WHEN RECORD_TYPE = 'SentForSettle' then COUNT(*)*$tax_authorised else COUNT(*)*$tax_received end as FEE_USD
from 
(
select *
,(CASE WHEN BOOKING_DATE::datetime between $hora_verano_ini::datetime and $hora_verano_fin::datetime 
  then dateadd(hour,5,BOOKING_DATE::datetime) else dateadd(hour,4,BOOKING_date::datetime) end )::DATE as ADYEN_TIME
from simetrikdb_public.Global__Payments__Accounting__Reports
where skt__uniqueness=1
--and merchant_account in ($acquirer)
and merchant_account in  (select value from table(flatten ( input => parse_json($variable1))))
)
where ADYEN_TIME::date between $fecha_ini::date and $fecha_fin::date
and RECORD_TYPE in ('SentForSettle','Received','SentForRefund','CaptureFailed')
--BOOKING_DATE::date between '2022-01-01'::date and '2022-01-31'::date
group by 1,2,3,4,5
--order by 1,2


UNION ALL

----------------- Authorisation Scheme fee---------------------

select
ADYEN_TIME::date as fecha
,merchant_account
,'02_Payment Method Fees' as Invoice_field
,'02_Payment Method Fees_Authorisation Scheme fee' as Invoice
,RECORD_TYPE
,COUNT(*) COUNT
,SUM(SCHEME_FEES_SC) as FEE_LOCAL
,SUM(SCHEME_FEES_SC)/$USD_CHANGE as FEE_USD
from 
(
select *
,(CASE WHEN BOOKING_DATE::datetime between $hora_verano_ini::datetime and $hora_verano_fin::datetime 
  then dateadd(hour,5,BOOKING_DATE::datetime) else dateadd(hour,4,BOOKING_date::datetime) end )::DATE as ADYEN_TIME
from simetrikdb_public.Global__Payments__Accounting__Reports
where skt__uniqueness=1
--and merchant_account = $acquirer
and merchant_account in  (select value from table(flatten ( input => parse_json($variable1))))
)
where ADYEN_TIME::date between $fecha_ini::date and $fecha_fin::date
and RECORD_TYPE in ('Cancelled','Expired','Refused','Retried')
group by 1,2,3,4,5  
  
  
UNION ALL  

------------Payment_method -  Commission Blend Cards (Banorte, visadebit, mcdebit, carnetdebit) (VolumeGroup: RappiVolume)   ------

select
ADYEN_TIME::date as fecha
,merchant_account
,'02_Payment Method Fees' as Invoice_field
,'03_Payment_method_Commission Blend Cards' as Invoice
,CASE WHEN PAYMENT_METHOD_VARIANT like '%debit%' or PAYMENT_METHOD_VARIANT = 'electron' then 'Commission Blend Cards_1'else 'Commission Blend Cards_2' end as PAYMENT_METHOD_VARIANT
,COUNT(*) COUNT
,SUM(COMMISSION_SC) as FEE_LOCAL
,SUM(COMMISSION_SC)/$USD_CHANGE as FEE_USD
from 
(
select *
,(CASE WHEN BOOKING_DATE::datetime between $hora_verano_ini::datetime and $hora_verano_fin::datetime 
  then dateadd(hour,5,BOOKING_DATE::datetime) else dateadd(hour,4,BOOKING_date::datetime) end )::DATE as ADYEN_TIME
from simetrikdb_public.Global__Payments__Accounting__Reports
where skt__uniqueness=1
--and merchant_account = $acquirer
and merchant_account in  (select value from table(flatten ( input => parse_json($variable1))))
)
where ADYEN_TIME::date between $fecha_ini::date and $fecha_fin::date
and RECORD_TYPE = 'Settled'
group by 1,2,3,4,5

  
UNION ALL
  
------------Payment_method - Interchange Issuing Banks Cards ------

select
ADYEN_TIME::date as fecha
,merchant_account
,'02_Payment Method Fees' as Invoice_field
,'04_Payment_method_Interchange Issuing Banks Cards' as Invoice
,RECORD_TYPE
,COUNT(*) COUNT
,SUM(INTERCHANGE_SC) as FEE_LOCAL
,SUM(INTERCHANGE_SC)/$USD_CHANGE as FEE_USD
from 
(
select *
,(CASE WHEN BOOKING_DATE::datetime between $hora_verano_ini::datetime and $hora_verano_fin::datetime 
  then dateadd(hour,5,BOOKING_DATE::datetime) else dateadd(hour,4,BOOKING_date::datetime) end )::DATE as ADYEN_TIME
from simetrikdb_public.Global__Payments__Accounting__Reports
where skt__uniqueness=1
--and merchant_account = $acquirer
and merchant_account in  (select value from table(flatten ( input => parse_json($variable1))))
)
where ADYEN_TIME::date between $fecha_ini::date and $fecha_fin::date
--and PAYMENT_METHOD in ('mc','visa')
and RECORD_TYPE in ('Settled')
group by 1,2,3,4,5  
  
  
UNION ALL
  
------------Payment_method -  Commission Markup Cards (VolumeGroup: RappiVolume)  ------

select
ADYEN_TIME::date as fecha
,merchant_account
,'02_Payment Method Fees' as Invoice_field
,'05_Payment_method_Commission Markup Cards' as Invoice
,RECORD_TYPE
,COUNT(*) COUNT
,SUM(MARKUP_SC) as FEE_LOCAL
,SUM(MARKUP_SC)/$USD_CHANGE as FEE_USD
from 
(
select *
,(CASE WHEN BOOKING_DATE::datetime between $hora_verano_ini::datetime and $hora_verano_fin::datetime 
  then dateadd(hour,5,BOOKING_DATE::datetime) else dateadd(hour,4,BOOKING_date::datetime) end )::DATE as ADYEN_TIME
from simetrikdb_public.Global__Payments__Accounting__Reports
where skt__uniqueness=1
--and merchant_account = $acquirer
and merchant_account in  (select value from table(flatten ( input => parse_json($variable1))))
)
where ADYEN_TIME::date between $fecha_ini::date and $fecha_fin::date
--and PAYMENT_METHOD in ('mc','visa')
and RECORD_TYPE in ('Settled')
group by 1,2,3,4,5


UNION ALL 
  
------------Payment_method - Scheme fee Visa & Mastercard Cards------

select
ADYEN_TIME::date as fecha
,merchant_account
,'02_Payment Method Fees' as Invoice_field
,'06_Payment_method_Scheme fee Visa & Mastercard Cards Payment' as Invoice
,RECORD_TYPE
,COUNT(*) COUNT
,SUM(SCHEME_FEES_SC) as FEE_LOCAL
,SUM(SCHEME_FEES_SC)/$USD_CHANGE as FEE_USD
from 
(
select *
,(CASE WHEN BOOKING_DATE::datetime between $hora_verano_ini::datetime and $hora_verano_fin::datetime 
  then dateadd(hour,5,BOOKING_DATE::datetime) else dateadd(hour,4,BOOKING_date::datetime) end )::DATE as ADYEN_TIME
from simetrikdb_public.Global__Payments__Accounting__Reports
where skt__uniqueness=1
--and merchant_account = $acquirer
and merchant_account in  (select value from table(flatten ( input => parse_json($variable1))))
  
)
where ADYEN_TIME::date between $fecha_ini::date and $fecha_fin::date
--and PAYMENT_METHOD in ('mc','visa')
and RECORD_TYPE in ('Settled')
group by 1,2,3,4,5


UNION ALL

----------------- Management Service -Management Fee for external settled acquirers---------------------
  
select
ADYEN_TIME::date as fecha
,merchant_account
,'03_Management Service' as Invoice_field
,'07_Management Service_Management Fee for external settled acquirers' as Invoice
,PAYMENT_METHOD
,COUNT(*) COUNT
,SUM(CAPTURED_PC)*$tax_mgmt_service as FEE_LOCAL
,SUM(CAPTURED_PC)*$tax_mgmt_service/$USD_CHANGE as FEE_USD
from 
(
select *
,(CASE WHEN BOOKING_DATE::datetime between $hora_verano_ini::datetime and $hora_verano_fin::datetime 
  then dateadd(hour,5,BOOKING_DATE::datetime) else dateadd(hour,4,BOOKING_date::datetime) end )::DATE as ADYEN_TIME
from simetrikdb_public.Global__Payments__Accounting__Reports
where skt__uniqueness=1
--and merchant_account = $acquirer
and merchant_account in  (select value from table(flatten ( input => parse_json($variable1))))  
)
where ADYEN_TIME::date between $fecha_ini::date and $fecha_fin::date
and PAYMENT_METHOD='amex'
and RECORD_TYPE='SentForSettle'
group by 1,2,3,4,5

  
UNION ALL
  
------------Refund Fees - Interchange Issuing Banks Cards ------

select
ADYEN_TIME::date as fecha
,merchant_account
,'04_Refund Fees' as Invoice_field
,'08_Refund Fees_Interchange Issuing Banks Cards Refunds' as Invoice
,RECORD_TYPE
,COUNT(*) COUNT
,SUM(INTERCHANGE_SC) as FEE_LOCAL
,SUM(INTERCHANGE_SC)/$USD_CHANGE as FEE_USD
from 
(
select *
,(CASE WHEN BOOKING_DATE::datetime between $hora_verano_ini::datetime and $hora_verano_fin::datetime 
  then dateadd(hour,5,BOOKING_DATE::datetime) else dateadd(hour,4,BOOKING_date::datetime) end )::DATE as ADYEN_TIME
from simetrikdb_public.Global__Payments__Accounting__Reports
where skt__uniqueness=1
--and merchant_account = $acquirer
and merchant_account in  (select value from table(flatten ( input => parse_json($variable1))))
)
where ADYEN_TIME::date between $fecha_ini::date and $fecha_fin::date
--and PAYMENT_METHOD in ('mc','visa')
and RECORD_TYPE in ('Refunded')
group by 1,2,3,4,5

  
UNION ALL

------------Refund Fees -  Commission Markup Cards (VolumeGroup: RappiVolume)  ------

select
ADYEN_TIME::date as fecha
,merchant_account
,'04_Refund Fees' as Invoice_field
,'09_Refund Fees_Commission Markup Cards Refunds' as Invoice
,RECORD_TYPE
,COUNT(*) COUNT
,SUM(MARKUP_SC) as FEE_LOCAL
,SUM(MARKUP_SC)/$USD_CHANGE as FEE_USD
from 
(
select *
,(CASE WHEN BOOKING_DATE::datetime between $hora_verano_ini::datetime and $hora_verano_fin::datetime 
  then dateadd(hour,5,BOOKING_DATE::datetime) else dateadd(hour,4,BOOKING_date::datetime) end )::DATE as ADYEN_TIME
from simetrikdb_public.Global__Payments__Accounting__Reports
where skt__uniqueness=1
--and merchant_account = $acquirer
and merchant_account in  (select value from table(flatten ( input => parse_json($variable1))))
)
where ADYEN_TIME::date between $fecha_ini::date and $fecha_fin::date
--and PAYMENT_METHOD in ('mc','visa')
and RECORD_TYPE in ('Refunded')
group by 1,2,3,4,5


UNION ALL

------------Refund Fees - Scheme fee Visa & Mastercard Cards------

select
ADYEN_TIME::date as fecha
,merchant_account
,'04_Refund Fees' as Invoice_field
,'10_Refund Fees_Scheme fee Visa & Mastercard Cards Refunds' as Invoice
,RECORD_TYPE
,COUNT(*) COUNT
,SUM(SCHEME_FEES_SC) as FEE_LOCAL
,SUM(SCHEME_FEES_SC)/$USD_CHANGE as FEE_USD
from 
(
select *
,(CASE WHEN BOOKING_DATE::datetime between $hora_verano_ini::datetime and $hora_verano_fin::datetime 
  then dateadd(hour,5,BOOKING_DATE::datetime) else dateadd(hour,4,BOOKING_date::datetime) end )::DATE as ADYEN_TIME
from simetrikdb_public.Global__Payments__Accounting__Reports
where skt__uniqueness=1
--and merchant_account = $acquirer
and merchant_account in  (select value from table(flatten ( input => parse_json($variable1))))
  
)
where ADYEN_TIME::date between $fecha_ini::date and $fecha_fin::date
--and PAYMENT_METHOD in ('mc','visa')
and RECORD_TYPE in ('Refunded')
group by 1,2,3,4,5    


UNION ALL

----------------- Chargeback Service---------------------
  
select
ADYEN_TIME::date as fecha
,merchant_account
,'05_Chargeback Service' as Invoice_field
,'11_Chargeback Service_Commission Blend Chargebacks' as Invoice
,PAYMENT_METHOD_VARIANT
,COUNT(*) COUNT
,COUNT(*)*5*$USD_CHANGE as FEE_LOCAL
,COUNT(*)*5 as FEE_USD
from 
(
select *
,(CASE WHEN BOOKING_DATE::datetime between $hora_verano_ini::datetime and $hora_verano_fin::datetime 
  then dateadd(hour,5,BOOKING_DATE::datetime) else dateadd(hour,4,BOOKING_date::datetime) end )::DATE as ADYEN_TIME
from simetrikdb_public.Global__Payments__Accounting__Reports
where skt__uniqueness=1
--and merchant_account = $acquirer
and merchant_account in  (select value from table(flatten ( input => parse_json($variable1))))
)
where ADYEN_TIME::date between $fecha_ini::date and $fecha_fin::date
and RECORD_TYPE in ('Chargeback','SecondChargeback')
and (COMMISSION_SC <>0 or COMMISSION_SC is not null)
and ((MARKUP_SC=0 and SCHEME_FEES_SC=0 and INTERCHANGE_SC=0)or (MARKUP_SC is null and SCHEME_FEES_SC is null and INTERCHANGE_SC is null))
group by 1,2,3,4,5

  
UNION ALL

----------------- Chargeback Service - Scheme fee Visa & Mastercard Chargebacks---------------------

select
ADYEN_TIME::date as fecha
,merchant_account
,'05_Chargeback Service' as Invoice_field
,'12_Chargeback Service_Commission Markup Chargebacks' as Invoice
,RECORD_TYPE
,COUNT(*) COUNT
,COUNT(*)*5*$USD_CHANGE as FEE_LOCAL
,COUNT(*)*5 as FEE_USD
from 
(
select *
,(CASE WHEN BOOKING_DATE::datetime between $hora_verano_ini::datetime and $hora_verano_fin::datetime 
  then dateadd(hour,5,BOOKING_DATE::datetime) else dateadd(hour,4,BOOKING_date::datetime) end )::DATE as ADYEN_TIME
from simetrikdb_public.Global__Payments__Accounting__Reports
where skt__uniqueness=1
--and merchant_account = $acquirer
and merchant_account in  (select value from table(flatten ( input => parse_json($variable1))))
)
where ADYEN_TIME::date between $fecha_ini::date and $fecha_fin::date
and RECORD_TYPE in ('Chargeback','SecondChargeback')
and (COMMISSION_SC =0 or COMMISSION_SC is null)
and ((MARKUP_SC<>0 and SCHEME_FEES_SC<>0 and INTERCHANGE_SC<>0)or (MARKUP_SC is not null and SCHEME_FEES_SC is not null and INTERCHANGE_SC is not null))
group by 1,2,3,4,5

  
UNION ALL

----------------- Chargeback Service - Scheme fee Visa & Mastercard Chargebacks---------------------

select
ADYEN_TIME::date as fecha
,merchant_account
,'05_Chargeback Service' as Invoice_field
,'13_Chargeback Service_Scheme fee Visa & Mastercard Chargebacks' as Invoice
,RECORD_TYPE
,COUNT(*) COUNT
,SUM(SCHEME_FEES_SC) as FEE_LOCAL
,SUM(SCHEME_FEES_SC)/$USD_CHANGE as FEE_USD
from 
(
select *
,(CASE WHEN BOOKING_DATE::datetime between $hora_verano_ini::datetime and $hora_verano_fin::datetime 
  then dateadd(hour,5,BOOKING_DATE::datetime) else dateadd(hour,4,BOOKING_date::datetime) end )::DATE as ADYEN_TIME
from simetrikdb_public.Global__Payments__Accounting__Reports
where skt__uniqueness=1
--and merchant_account = $acquirer
and merchant_account in  (select value from table(flatten ( input => parse_json($variable1))))
)
where ADYEN_TIME::date between $fecha_ini::date and $fecha_fin::date
and RECORD_TYPE in ('Chargeback','SecondChargeback')
and (COMMISSION_SC =0 or COMMISSION_SC is null)
and ((MARKUP_SC<>0 and SCHEME_FEES_SC<>0 and INTERCHANGE_SC<>0)or (MARKUP_SC is not null and SCHEME_FEES_SC is not null and INTERCHANGE_SC is not null))
group by 1,2,3,4,5


)

Select 
fecha
,'MX' as pais
,'Adyen' as adquiriente
,merchant_account
,Invoice_field
,RECORD_TYPE
,0 as VENTA_LOCAL
,0 as VENTA_USD
,SUM(FEE_LOCAL) as FEE_LOCAL
,SUM(FEE_USD) as FEE_USD
from adyen_fees
where Invoice_field <>'05_Chargeback Service'
group by 1,2,3,4,5,6
order by 1,2,3,4,5,6