set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=20;
select 
skt__TRANSACTION_INITIATION_DATE::Date as fecha
,'MX' as pais
,'Paypal' as adquiriente
,case when TRANSACTION_EVENT_CODE = 'T0003' then 'ventas' else 'refunds' end as tipo
,sum(case when TRANSACTION_EVENT_CODE = ('T0003') then GROSS_TRANSACTION_AMOUNT/100 else (-GROSS_TRANSACTION_AMOUNT/100) end) as VENTA_LOCAL
,sum(case when TRANSACTION_EVENT_CODE = ('T0003') then GROSS_TRANSACTION_AMOUNT/100 else (-GROSS_TRANSACTION_AMOUNT/100) end)/$ex_rate as VENTA_USD
,sum(case when TRANSACTION_EVENT_CODE = ('T0003') then FEE_AMOUNT/100 else (-FEE_AMOUNT/100) end) as FEE_LOCAL
,sum(case when TRANSACTION_EVENT_CODE = ('T0003') then FEE_AMOUNT/100 else (-FEE_AMOUNT/100) end)/$ex_rate as FEE_USD
from simetrikdb_public.MX__adquirencia__paypal 
where TRANSACTION_EVENT_CODE in  ('T1107','T0003','T1106')
and skt__TRANSACTION_INITIATION_DATE::Date between $fecha_ini::date and $fecha_fin::date
and SKT__UNIQUENESS = 1
group by 1,2,3,4
order by 1,2,3,4
/*
;

select *
select sum(insurance_amount), sum(sales_tax_amount), sum(shipping_amount)
from simetrikdb_public.MX__adquirencia__paypal 
where skt__TRANSACTION_INITIATION_DATE::Date between '2022-01-01'::date and '2022-04-30'::date
*/