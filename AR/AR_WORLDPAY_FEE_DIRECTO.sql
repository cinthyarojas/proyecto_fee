set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=65;
select 
SKT__SETTLEMENT_DATE::date as fecha
,'AR' as pais
,'WORLDPAY' as adquiriente
,SUM(TRY_CAST(SALES_AMOUNT_OR_INSTALLMENT_AMOUNT as DOUBLE)/100) as VENTA_LOCAL
,SUM(TRY_CAST(SALES_AMOUNT_OR_INSTALLMENT_AMOUNT as DOUBLE)/100)/$ex_rate as VENTA_USD
,SUM(TRY_CAST(TRANSACTION_FEES as DOUBLE )/100) as FEE_LOCAL
,SUM(TRY_CAST(TRANSACTION_FEES as DOUBLE )/100)/$ex_rate as FEE_USD
,SUM(TRY_CAST(NET_SALES_AMOUNT as DOUBLE )/100) as abono 
from( 
    select distinct merchant_reference_number as llave, skt__date_of_transaction,SKT__SETTLEMENT_DATE,record_type,sales_amount_or_installment_amount,
    transaction_fees,merchant_iva,ganancias_tax,vat_over_msc,net_sales_amount,skt__uniqueness,MERCHANT_REFERENCE_NUMBER
    from SIMETRIKDB_PUBLIC.AR__SALES__WP2
    )
where SKT__SETTLEMENT_DATE::date between $fecha_ini::date and $fecha_fin::date
and skt__uniqueness = 1
and record_type = 2
group by 1,2,3
order by 1