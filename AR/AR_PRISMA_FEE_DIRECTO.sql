set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=65;
select 
SKT__FPAG::date as fecha
,'AR' as pais
,'PRISMA' as adquiriente
,SUM(IMPORTE) as VENTA_LOCAL
,SUM(IMPORTE)/$ex_rate as VENTA_USD
,SUM(ARANCEL) as FEE_LOCAL
,SUM(ARANCEL)/$ex_rate as FEE_USD
from 
SIMETRIKDB_PUBLIC.AR__ADQUIRENCIA__PRISMA
where SKT__FPAG::date between $fecha_ini::date and $fecha_fin::date
group by 1,2,3
order by 1