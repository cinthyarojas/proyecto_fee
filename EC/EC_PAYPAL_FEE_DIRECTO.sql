set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
select 
SKT_FECHA_TRANSACCION::date as fecha
,'EC' as pais
,'Paypal' as adquiriente
,SUM(IMPORTE_BRUTO_DE_LA_TRANSACCION) as VENTA_LOCAL
,SUM(IMPORTE_BRUTO_DE_LA_TRANSACCION) as VENTA_USD
,SUM(IMPORTE_DE_LA_COMISION)/100  as FEE_LOCAL
,SUM(IMPORTE_DE_LA_COMISION)/100  as FEE_USD
from 
simetrikdb_public.EC_ADQ_PAYPAL_8670 
where SKT__UNIQUENESS=1
and SKT_FECHA_TRANSACCION::date between $fecha_ini::date and $fecha_fin::date
group by 1,2,3
order by 1
