set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
select 
APPROVED_DATE::date as fecha
,'EC' as pais
,'DLOCAL' as adquiriente
,sum(GROSS_LOCAL_AMOUNT) AS VENTA_LOCAL
,sum(GROSS_LOCAL_AMOUNT) AS VENTA_USD
,sum(fee_local_amount) as Fee_LOCAL
,sum(fee_local_amount) as Fee_USD
,SUM(TAX_LOCAL_AMOUNT) AS Taxes
from SIMETRIKDB_PUBLIC.ec_adquirencia_dlocal_7354
where APPROVED_DATE::date between  $fecha_ini::DATE and $fecha_fin::DATE
and SKT__UNIQUENESS = 1
group by 1,2,3
order by 1