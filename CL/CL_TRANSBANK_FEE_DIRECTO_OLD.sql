set fecha_ini='2021-12-01';
set fecha_fin='2022-03-31';
SELECT MES,SUM(ABONOS) ABONOS,SUM(ANULACION) ANULACIONES,(SUM(FEE_IVA)-SUM(DEV_FEE_IVA)) AS FEE_IVA,SUM(FEE)-SUM(DEV_FEE) AS FEE,SUM(IVA)-SUM(DEV_IVA) AS IVA 
FROM (
select date_trunc('mm',SKT__FECHA_ABONO::Date) as mes,
    SUM(replace(MONTO_PARA_ABONO, '.', '')::DOUBLE) as ABONOS,
    SUM(replace(COMISION_E_IVA_COMISION, '.', '')::DOUBLE) as FEE_IVA,
    SUM(replace(MONTO_PARA_ABONO, '.', '')::DOUBLE)*0.0239 AS FEE,
    SUM(replace(COMISION_E_IVA_COMISION, '.', '')::DOUBLE)-(SUM(replace(MONTO_PARA_ABONO, '.', '')::DOUBLE)*0.0239) AS IVA,
    SUM(replace(MONTO_RETENCION, '.', '')::DOUBLE) as ANULACION,
    SUM(replace(MONTO_RETENCION, '.', '')::DOUBLE)*0.0239 AS DEV_FEE,
    SUM(replace(DEVOLUCION_COMISION_E_IVA_COMISION, '.', '')::DOUBLE) as DEV_FEE_IVA,
    SUM(replace(DEVOLUCION_COMISION_E_IVA_COMISION, '.', '')::DOUBLE)-(SUM(replace(MONTO_RETENCION, '.', '')::DOUBLE)*0.0239) AS DEV_IVA
    
from simetrikdb_public.CL__ABONOS__CREDITOS__TRANSBANK__ADMIN
where SKT__FECHA_ABONO::Date between '2021-08-01'and '2021-08-31'
and skt__uniqueness = 1
group by 1
UNION ALL 
select date_trunc('mm',SKT__FECHA_ABONO::Date) as mes,
    SUM(replace(MONTO_TRANSACCION, '.', '')::DOUBLE) as ABONOS,
    SUM(replace(MONTO_TRANSACCION, '.', '')::DOUBLE)*0.014294 AS FEE,
    SUM(replace(COMISION_E_IVA_COMISION, '.', '')::DOUBLE) as FEE_IVA,
    SUM(replace(COMISION_E_IVA_COMISION, '.', '')::DOUBLE)-(SUM(replace(MONTO_TRANSACCION, '.', '')::DOUBLE)*0.014294) AS IVA,
    SUM(replace(MONTO_RETENCION, '.', '')::DOUBLE) as ANULACION,
    SUM(replace(MONTO_RETENCION, '.', '')::DOUBLE)*0.014294 AS DEV_FEE,
    SUM(replace(DEVOLUCION_COMISION, '.', '')::DOUBLE) as DEV_FEE_IVA,
    SUM(replace(DEVOLUCION_COMISION, '.', '')::DOUBLE)-(SUM(replace(MONTO_ANULACION, '.', '')::DOUBLE)*0.014294) AS DEV_IVA
from simetrikdb_public.CL__ABONOS__DEBITOS__TRANSBANK__ADMIN  
where SKT__FECHA_ABONO::Date between '2021-09-01'and '2021-09-30'
and skt__uniqueness = 1
group by 1
)
GROUP BY 1