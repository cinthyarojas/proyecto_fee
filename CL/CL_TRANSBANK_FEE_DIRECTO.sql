set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=800;
select 
SKT__FECHA_VENTA::date as fecha
,'CL' as pais
,'TRANSBANK' as adquiriente
,'Creditos' as type
,MOTIVO
,tipo_transaccion 
,SUM(MONTO_ANULACION) as VENTA_LOCAL
,SUM(MONTO_ANULACION)/$ex_rate as VENTA_USD
,-SUM(COMISION_E_IVA_COMISION) as FEE
,-SUM(COMISION_ADICIONAL_E_IVA_COMISION_ADICIONAL) as FEE_ADICIONAL 
,(FEE+FEE_ADICIONAL)/1.19 as FEE_LOCAL
,FEE_LOCAL/$ex_rate as  FEE_USD
from
simetrikdb_public.CL__ABONOS__CREDITOS__TRANSBANK__ADMIN
where skt__uniqueness = 1
and SKT__FECHA_VENTA between $fecha_ini::date and $fecha_fin::date
group by 1,2,3,4,5,6

union all

select 
SKT__FECHA_VENTA::date as fecha
,'CL' as pais
,'TRANSBANK' as adquiriente
,'Debitos' as type
,MOTIVO
,tipo_transaccion
,SUM(MONTO_TRANSACCION) as VENTA_LOCAL
,SUM(MONTO_TRANSACCION)/$ex_rate as VENTA_USD
,SUM(COMISION_E_IVA_COMISION) as FEE
,0 AS FEE_ADICIONAL
,(FEE+FEE_ADICIONAL)/1.19 as FEE_LOCAL
,FEE_LOCAL/$ex_rate as FEE_USD
from simetrikdb_public.CL__ABONOS__DEBITOS__TRANSBANK__ADMIN
where SKT__UNIQUENESS = 1
and SKT__FECHA_VENTA between $fecha_ini::date and $fecha_fin::date
group by 1,2,3,4,5,6
