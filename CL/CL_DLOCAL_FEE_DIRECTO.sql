set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=800;
select
APPROVED_DATE::date as Fecha
,'CL' as pais
,'Dlocal' as adquiriente
,sum(GROSS_LOCAL_AMOUNT) AS VENTA_LOCAL
,sum(GROSS_LOCAL_AMOUNT)/$ex_rate AS VENTA_USD
,sum(fee_local_amount) as FEE_LOCAL
,sum(fee_local_amount)/$ex_rate as FEE_USD
--,SUM(TAX_LOCAL_AMOUNT) AS Taxes
--, sum(net_local_amount) as Venta_neta
from SIMETRIKDB_PUBLIC.cl_adquirencia_dlocal_6563
where APPROVED_DATE::date between  $fecha_ini::DATE and $fecha_fin::DATE
and SKT__UNIQUENESS = 1
group by 1,2,3
order by 1,2,3