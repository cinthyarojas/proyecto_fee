set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=800;
select 
FECHA_VENTA::date as fecha
,'CL' as pais
,'Klap' as adquiriente
,tipo_tx
,CASE WHEN tipo_tx ilike '%VENTA%' then SUM(TOTAL) else -SUM(TOTAL) end as VENTA_LOCAL
,(CASE WHEN tipo_tx ilike '%VENTA%' then SUM(TOTAL) else -SUM(TOTAL) end)/$ex_rate as VENTA_USD
,CASE WHEN tipo_tx ilike '%VENTA%' then SUM(COMISION) else -SUM(COMISION) end as FEE_LOCAL
,(CASE WHEN tipo_tx ilike '%VENTA%' then SUM(COMISION) else -SUM(COMISION) end )/$ex_rate as  FEE_USD
from SIMETRIKDB_PUBLIC.cl__adquirencia__klap
where FECHA_VENTA::date between $fecha_ini::date and $fecha_fin::date
and SKT__UNIQUENESS =1
group by 1,2,3,4
order by 1,2