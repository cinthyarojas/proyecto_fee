set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=3.4;
SELECT 
transaction_date::Date as FECHA
,'PE' as PAIS
,'PayU' as Adquiriente
,MERCHANT_ID
,TYPE
,SUM(amount_payment) as VENTA_LOCAL
,SUM(amount_payment)/$ex_rate as VENTA_USD
,SUM(CASE WHEN state = 'APPROVED' AND TYPE IN ('CAPTURE','AUTHORIZATION_AND_CAPTURE' ) THEN amount_payment*0.028 END) AS FEE_LOCAL
,SUM(CASE WHEN state = 'APPROVED' AND TYPE IN ('CAPTURE','AUTHORIZATION_AND_CAPTURE' ) THEN amount_payment*0.028 END)/$ex_rate as FEE_USD
FROM simetrikdb_public.pe__adquirencia__payu__sftp         
WHERE transaction_date::Date BETWEEN  $fecha_ini::date AND $fecha_fin::date
and skt__uniqueness = 1
and MERCHANT_ID ='701706'
and state = 'APPROVED' 
group by 1,2,3,4,5
order by 1
