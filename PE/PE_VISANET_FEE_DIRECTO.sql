set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=3.4;
select 
SKT__FECHA_TRANSACCION::date as fecha
,'PE' as PAIS
,CASE WHEN CODIGO_COMERCIO in ('650192145','650191977') then 'Visanet_pay' else 'Visanet' end as adquiriente
,TIPO_TRANSACCION
,CODIGO_COMERCIO
,SUM(IMPORTE_TRANSACCION) as VENTA_LOCAL
,SUM(IMPORTE_TRANSACCION)/$ex_rate as VENTA_USD
,SUM(COMISION_VISA)+SUM(COMISION_IGV) as FEE_LOCAL
,(SUM(COMISION_VISA)+SUM(COMISION_IGV))/$ex_rate as FEE_USD
from SIMETRIKDB_PUBLIC.PE__ADQUIRENCIA__VISANET__SFTP
where SKT__FECHA_TRANSACCION::date
between $fecha_ini::date and $fecha_fin::date
and SKT__UNIQUENESS = 1
group by 1,2,3,4,5
order by 1,2,3
