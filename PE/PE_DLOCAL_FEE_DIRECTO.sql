set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=3.4;
select 
APPROVED_DATE::date as FECHA
,'PE' as PAIS
,'Dlocal' as adquiriente
,ROW_TYPE
,SUM(GROSS_LOCAL_AMOUNT) as VENTA_LOCAL
,SUM(GROSS_LOCAL_AMOUNT)/$ex_rate as VENTA_USD
,SUM(FEE_LOCAL_AMOUNT) as FEE_LOCAL
,SUM(FEE_LOCAL_AMOUNT)/$ex_rate as FEE_USD
from 
SIMETRIKDB_PUBLIC.PE__ADQUIRENCIA__DLOCAL
where APPROVED_DATE::date between $fecha_ini::date and $fecha_fin::date
and SKT__UNIQUENESS = 1
group by 1,2,3,4
order by 1
