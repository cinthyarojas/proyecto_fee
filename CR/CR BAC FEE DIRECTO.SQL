set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=575;
SELECT 
skt__fecha_pago::date as fecha
,'CR' as pais
,'BAC' as adquiriente
,sum(monto_venta) AS VENTA_LOCAL
,sum(monto_venta)/$ex_rate AS VENTA_USD
,sum(comision) AS FEE_LOCAL
,sum(comision)/$ex_rate AS FEE_USD
--sum(retencion_ventas) AS RETE_VENTAS
--sum(retencion_renta) AS RETE_RENTA
--sum(monto_neto) AS VENTA_NETA
from simetrikdb_public.cr__adquirencia__bac
where SKT__UNIQUENESS = 1 
and skt__fecha_pago::date between $fecha_ini::date and $fecha_fin::date
group by 1,2
order by 1