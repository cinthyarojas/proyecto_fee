set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=3530;
select 
SKT__FECHA_DE_CONSIGNACION as fecha
,'CO' as pais
,'Olimpica' as adquiriente
,SUM(VALOR_COMPRA) as VENTA_LOCAL
,SUM(VALOR_COMPRA)/$ex_rate as VENTA_USD
,SUM(VALOR_COMISION_ADQUIRENTE) as FEE_LOCAL
,SUM(VALOR_COMISION_ADQUIRENTE)/$ex_rate as FEE_USD
from simetrikdb_public.CO__ADQUIRENCIA__OLIMPICA
where SKT__FECHA_DE_CONSIGNACION::date between $fecha_ini::date and $fecha_fin::date
and skt__uniqueness = 1
group by 1,2,3
order by 1,2,3
