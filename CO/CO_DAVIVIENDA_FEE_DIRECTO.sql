set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=3530;
select
case when trn in (6,410) and skt__abono_date::Date < '2018-12-31' then '2018-12-31' else skt__abono_date::Date end as fecha
,'CO' as pais
,'Davivienda' as adquiriente
--,commerce
,case when commerce IN ('16529786.0','16529786','1652978') then 'Pay' else 'Rappi' end as Empresa
--,franchise
,sum(case when trn in (6,78,410) then total_amount else -total_amount end ) as VENTA_LOCAL
,sum(case when trn in (6,78,410) then total_amount else -total_amount end )/$ex_rate as VENTA_USD
,sum(case when trn in (6,78,410) then -commision_amount else commision_amount end) as FEE_LOCAL
,(sum(case when trn in (6,78,410) then -commision_amount else commision_amount end))/$ex_rate as FEE_USD
--,skt__abono_date::Date as mes_abonado
/*,account
,trn
,commerce
,case when commerce IN ('16529786.0','16529786','1652978') then 'Pay' else 'Rappi' end as Empresa
,franchise
,case when trn in (6,410) then 'Ventas'
when trn in (26,420) then 'Refunds'
else 'Cbks' end as tipo,
case when trn in (6,78,410) then 'Debito' else 'Credito' end as tipo_banco,
sum(case when trn in (6,78,410) then total_amount else -total_amount end ) as bruto,

sum(case when trn in (6,78,410) then -reteiva_amount else reteiva_amount end ) as reteiva,
sum(case when trn in (6,78,410) then -reteica_amount else reteica_amount end) as reteica ,
sum(case when trn in (6,78,410) then -retefuente_amount else retefuente_amount end ) as retefuente,
sum(case when trn in (6,78,410) then abono_amount else -abono_amount end ) as abono
*/
from SIMETRIKDB_PUBLIC.CO__ADQUIRENCIA__DAVIVIENDA
where skt__abono_date::date between $fecha_ini::Date and $fecha_fin::date
--where SKT__VOUCHER_DATE::date between $fecha_ini::Date and $fecha_fin::date
group by 1,2,3,4
order by 2 asc, 3 desc, 1