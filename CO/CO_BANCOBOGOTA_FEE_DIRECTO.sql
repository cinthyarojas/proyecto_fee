set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=3530;
select  
fec_tr::date as fecha
,'CO' as pais
,'Banco_bogota' as adquiriente
--,fec_consi::date
,TR
--,tipo
--, origen
--, cuenta
--, tr
--, codigo
,sum(case when tr in (6,10) then compra else -compra end) as VENTA_LOCAL
,sum(case when tr in (6,10) then compra else -compra end)/$ex_rate as VENTA_USD
,sum(case when tr in (6,10) then comision else -comision end) as FEE_LOCAL 
,sum(case when tr in (6,10) then comision else -comision end)/$ex_rate as FEE_USD
/*
sum(case when tr in (6,10) then iva else -iva end) as iva, 
sum(case when tr in (6,10) then reterenta else -reterenta end) as reterenta, 
sum(case when tr in (6,10) then reteiva else -reteiva end) as reteiva,
sum(case when tr in (6,10) then reteica else -reteica end) as reteica,
*/
from simetrikdb_public.co__adquirencia__banco__bogota
where fec_tr::date between $fecha_ini::Date and $fecha_fin::date
and skt__uniqueness = 1
and file_name not ilike '%ventas%'
group by 1,2,3,4
order by 1,2,3,4