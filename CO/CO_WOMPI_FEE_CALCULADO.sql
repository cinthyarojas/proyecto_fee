set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=3530;
select 
FECHA::date as fecha
,'CO' as pais
,'Wompi' as adquiriente
,SUM(AMOUNT) as VENTA_LOCAL
,(SUM(AMOUNT))/$ex_rate as VENTA_USD
,SUM(case WHEN (AMOUNT<=40000) then (AMOUNT*0.015) else 600 end) AS FEE_LOCAL
,(SUM(case WHEN (AMOUNT<=40000) then (AMOUNT*0.015) else 600 end))/$ex_rate AS FEE_USD
from SIMETRIKDB_PUBLIC.CO__DISBURSEMENT__WOMPI
where SKT__UNIQUENESS=1
and FECHA::date between $fecha_ini::date and $fecha_fin::date
group by 1,2,3
order by 1
