set fecha_ini='2022-01-01';
set fecha_fin='2022-04-25';
set ex_rate=3530;
select
CREATED::date as FECHA
,'CO' as pais
,'Stripe_ned' as adquiriente
,REPORTING_CATEGORY
,SUM(AMOUNT) as VENTA_LOCAL
,SUM(AMOUNT)/$ex_rate as VENTA_USD
,SUM(FEE) as FEE_LOCAL
,SUM(FEE)/$ex_rate as FEE_USD
from simetrikdb_public.co__acquirement__stripe__holanda 
where CREATED::date between $fecha_ini::date and $fecha_fin::date
group by 1,2,3,4
order by 1,2,3,4
